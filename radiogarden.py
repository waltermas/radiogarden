#Get all country and cities
# http://radio.garden/api/ara/content/places

#Get info of city and Stations by city (last id is id_city)
# http://radio.garden/api/ara/content/page/W9g0lZfQ

#Get info of specific station (last id is id_station)
# http://radio.garden/api/ara/content/channel/tMGsmGxF

#Example Stream URL (id is id_station)
# http://radio.garden/api/ara/content/listen/tMGsmGxF/channel.mp3

# todas las radios de una ciudad
# curl http://radio.garden/api/ara/content/page/YsXtUw33/channels | jq '.'

  
import json, urllib.request
import sys



with open('places.json') as json_file:
    data = json.load(json_file)

# print(data["data"]["list"][0].keys())
# dict_keys(['id', 'title', 'country', 'url', 'size', 'boost', 'geo'])

localidad = sys.argv[1]

title = next ((x for x in data["data"]["list"] if x["title"]==localidad), 0)
if title == 0:
    # pruebo reemplazando espacios por 0xa0
    localidad2 = localidad.replace(' ',' ')
    title = next ((x for x in data["data"]["list"] if x["title"]==localidad2), 0)
    if title == 0:
        print("no hay resultados")
        exit(0)
    # print(title)

url = "http://radio.garden/api/ara/content/page/%s" % title["id"]
# print(url)

with urllib.request.urlopen(url) as u:
    citidata = json.loads(u.read().decode())


if len(citidata["data"]["content"][0]["items"]) > 5:
   urlAllStationsId = citidata["data"]["content"][0]["items"][6]["page"]["url"].split("/")[3]

   urlAllStations = "http://radio.garden/api/ara/content/page/%s/channels" % urlAllStationsId
   with urllib.request.urlopen(urlAllStations) as u:
        citidata2 = json.loads(u.read().decode())
        for item2 in citidata2["data"]["content"][0]["items"]:
            print(item2["title"])
            idRadio2 = item2["href"].split("/")[3]
            print("http://radio.garden/api/ara/content/listen/%s/channel.mp3" % idRadio2)
            print()


else:
    for item in citidata["data"]["content"][0]["items"]:
        # print(item)
        if not "page" in item:
            print(item["title"])
            idRadio = item["href"].split("/")[3]
            print("http://radio.garden/api/ara/content/listen/%s/channel.mp3" % idRadio)
            print()
     
